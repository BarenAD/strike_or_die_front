import React from 'react';
import "../style/MagazineComponent.sass";
import MagazineComponent from "./MagazineComponent";
import SelectAntagonist from "./SelectAntagonist";
import GameMainComponent from "./GameMainComponent";

export default class MainComponent extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            page: "main",
            save: {
                armor: null,
                weapon: null,
                money: 0
            },
            selected_antagonist: {
                armor: null,
                weapon: null
            },
            old_game_hp_players: null
        };
    }

    componentDidMount()
    {
        this.props.Model.load_save((response) => {this.handle_get_data_save(response)});
    }

    handle_reset_state()
    {
        this.setState({
            selected_antagonist: {
                armor: null,
                weapon: null
            },
            old_game_hp_players: null
        });
    }

    handle_get_data_save(response)
    {
        this.handle_change_save({
            armor: response.player_armor,
            weapon: response.player_weapon,
            money: response.player_money
        });
        if (response.old_game){
            this.handle_change_antagonist({
               armor: response.computer_armor,
               weapon: response.computer_weapon
            });
            this.setState({
                old_game_hp_players: {
                    playerHP: response.player_hp,
                    computerHP: response.computer_hp
                },
                page: "game"
            });
        }
    }

    handle_change_antagonist(in_object)
    {
        if(in_object.armor !== null && in_object.armor !== undefined){
            this.state.selected_antagonist.armor = in_object.armor;
        }
        if(in_object.weapon !== null && in_object.weapon !== undefined){
            this.state.selected_antagonist.weapon = in_object.weapon;
        }
        this.setState({selected_antagonist: this.state.selected_antagonist});
    }

    handle_change_save(in_object)
    {
        if(in_object.armor !== null && in_object.armor !== undefined){
            this.state.save.armor = in_object.armor;
        }
        if(in_object.weapon !== null && in_object.weapon !== undefined){
            this.state.save.weapon = in_object.weapon;
        }
        if(in_object.money !== null && in_object.money !== undefined){
            this.state.save.money += in_object.money;
            if (this.state.save.money < 0){
                this.state.save.money = 0;
            }
        }
        this.setState({save: this.state.save});
    }


    handle_change_page(new_page)
    {
        this.setState({page: new_page});
    }

    render()
    {
        return(
            <div style={{width: "100%", height: "100%"}}>
                {this.state.page === "main" &&
                    <div style={{height: "100%", width: "100%", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center"}}>
                        <button onClick={() => this.handle_change_page("magazine")}>магазин</button>
                        <button onClick={() => this.handle_change_page("select_antagonist")}>выбор противника</button>
                        <button onClick={() => this.props.Model.logout()}>ВЫЙТИ</button>
                    </div>
                }
                {this.state.page === "magazine" &&
                    <MagazineComponent
                        Model = {this.props.Model}
                        change_page = {(new_page) => {this.handle_change_page(new_page)}}
                        change_save = {(in_object) => {this.handle_change_save(in_object)}}
                        player_money = {this.state.save.money}
                    />
                }
                {this.state.page === "select_antagonist" &&
                    <SelectAntagonist
                        Model = {this.props.Model}
                        change_page = {(new_page) => {this.handle_change_page(new_page)}}
                        change_antagonist = {(in_object) => {this.handle_change_antagonist(in_object)}}
                    />
                }
                {this.state.page === "game" &&
                    <GameMainComponent
                        save = {this.state.save}
                        selectedAntagonist = {this.state.selected_antagonist}
                        oldGameHPPlayers = {this.state.old_game_hp_players}
                        Model = {this.props.Model}
                        change_page = {() => {this.handle_change_page("main")}}
                        change_save = {(in_object) => {this.handle_change_save(in_object)}}
                        reset_state = {() => {this.handle_reset_state()}}
                    />
                }
            </div>
        );
    }
}
