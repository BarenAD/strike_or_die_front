import React from 'react';
import "../style/AuthorizationComponent.sass";





export default  class Authorization extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            login:'',
            password:'',
            page_login: true,
            name: "",
            age: null,
            phone:"",
            reg_password:""
        }
    }

    handle_login(e){
    e.preventDefault();
        this.props.model.login(this.state.login,this.state.password);
    }

    handle_kek(e)
    {
        e.preventDefault();
        this.props.model.registration(
            this.state.name,
            parseInt(this.state.age),
            this.state.reg_password,
            this.state.phone
        );
    }

    render(){
        return(
            <div className='form-container'>
                <div style={{display: "flex"}}>
                    <button onClick={() => {this.setState({page_login: !this.state.page_login})}}>
                        {this.state.page_login ?
                            <p>LOGIN</p>
                            :
                            <p>REGISTRATION</p>
                        }
                    </button>
                </div>
                {this.state.page_login &&
                    <form className='form'>
                        <input type="text" placeholder={"phone"} className="login" onChange={(e)=>{this.setState({login:e.target.value})}}></input>
                        <input type="password" placeholder={"password"} className="password" onChange={(e)=>{this.setState({password:e.target.value})}}></input>
                        <input type="submit" className='submitButton' onClick={(event)=>this.handle_login(event)}></input>
                    </form>
                }
                {!this.state.page_login &&
                    <form className='form'>
                        <input type="text" placeholder={"name"} className="login" onChange={(e)=>{this.setState({name:e.target.value})}}></input>
                        <input type="text" placeholder={"age"} className="login" onChange={(e)=>{this.setState({age:e.target.value})}}></input>
                        <input type="text" placeholder={"phone"} className="login" onChange={(e)=>{this.setState({phone:e.target.value})}}></input>
                        <input type="password" placeholder={"password"} className="password" onChange={(e)=>{this.setState({reg_password:e.target.value})}}></input>
                        <input type="submit" className='submitButton' onClick={(event)=>this.handle_kek(event)}></input>
                    </form>
                }

            </div>
        )


    }

}
