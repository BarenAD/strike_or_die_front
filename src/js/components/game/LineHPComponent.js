import React from 'react';
import "../../style/game/LineHPComponent.sass";

export default class LineHPComponent extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return(
            <div className={"MainLineHPContainer"}>
                <div className={"DynamicContainerLine"} style={{height: (100-this.props.HP) + "%"}} />
                <div className={"StaticContainerLine"}/>
            </div>
        );
    }
}
