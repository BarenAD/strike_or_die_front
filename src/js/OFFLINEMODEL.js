export default class OFFLINEMODEL
{
    constructor(in_change_authorization, in_visible_login_form)
    {
        this.change_authorization = in_change_authorization;
        this.visible_login_form = in_visible_login_form;
    }

    check_authorization()
    {
        if((localStorage["auth_token"] !== undefined && localStorage["auth_token"] !== null) &&
        (localStorage["auth_login"] !== undefined && localStorage["auth_login"] !== null)) {
            if (localStorage["auth_token"] === "%71286@@*%!)((&@%" && localStorage["auth_login"] === "developer"){
                setTimeout(() => {this.change_authorization(true); this.visible_login_form(false);}, 500,);
            } else {
                setTimeout(() => {this.change_authorization(false); this.visible_login_form(true);}, 500,);
            }
        } else {


            setTimeout(() => {this.change_authorization(false); this.visible_login_form(true);}, 500,);
        }
    }

    login(login, password)
    {
        if(password === '123' && login === 'developer'){
            localStorage["auth_token"] = "%71286@@*%!)((&@%";
            localStorage["auth_login"] = "developer";
            setTimeout(() => {this.change_authorization(true); this.visible_login_form(false);}, 500,);
        }
    }

    registration(login, password)
    {

    }

    novikov_magazine_get_assortiment(CALLBACK)
    {
        setTimeout(CALLBACK([
            {
                id: 1,
                name: "Деревянный меч",
                type: "weapon",
                parameter: 100,
                price: 300
            },
            {
                id: 2,
                name: "Железный меч",
                type: "weapon",
                parameter: 300,
                price: 800
            },
            {
                id: 3,
                name: "Деревянная броня",
                type: "armor",
                parameter: 10,
                price: 1000
            },
            {
                id: 4,
                name: "Железная броня",
                type: "armor",
                parameter: 30,
                price: 5000
            }
        ]), 500,);
    }

    novikov_antagonist_get_enemies(CALLBACK)
    {
        setTimeout(CALLBACK([
            {
                id: 1,
                name: "Антоха",
                weapon: "Деревянный меч",
                armor: "Деревянная броня"
            },
            {
                id: 2,
                name: "Алек",
                weapon: "Деревянный меч",
                armor: "Деревянная броня"
            },
            {
                id: 3,
                name: "Алексей",
                weapon: "Железный меч",
                armor: "Деревянная броня"
            },
            {
                id: 4,
                name: "Жора",
                weapon: "Железный меч",
                armor: "Железная броня"
            }
        ]), 500,);
    }

    malashin_handle_send_step(in_attack, in_block, CALLBACK)
    {
        let Response = {
            successful_attack: null, //boolean
            successful_block: null, //boolean
            PlayerHP: 100, //integer
            ComputerHP: 100, //integer
            end_game: null,
            prize: null
        };
        let Associate = ["head", "tors", "legs"];
        let CorrectAttack = false;
        if (Associate.find(direction => direction === in_attack) !== null) {
            CorrectAttack = true;
        }
        let ComputerAttack = Associate[Math.floor(Math.random() * 3)];
        let ComputerBlock = Associate[Math.floor(Math.random() * 3)];
        if (in_attack === ComputerBlock || !CorrectAttack){
            Response.successful_attack = false;
        } else {
            Response.successful_attack = true;
        }
        if (in_block === ComputerAttack){
            Response.successful_block = true;
        } else {
            Response.successful_block = false;
        }
        if (Response.ComputerHP <= 0){
            Response.end_game = "player";
            Response.prize = 500;
        } else if (Response.PlayerHP <= 0) {
            Response.end_game = "computer";
            Response.prize = -500;
        }
        setTimeout(() => {CALLBACK(Response)}, 1000);
    }

}
