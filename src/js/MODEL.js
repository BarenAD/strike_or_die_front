export default class MODEL
{
    constructor(in_change_authorization, in_visible_login_form)
    {
        this.BackEndDomain = "http://localhost:8080/deploy/api";
        this.change_authorization = in_change_authorization;
        this.visible_login_form = in_visible_login_form;
    }

    check_authorization()
    {
        if((localStorage["auth_token"] !== undefined && localStorage["auth_token"] !== null) &&
            (localStorage["auth_id"] !== undefined && localStorage["auth_id"] !== null)) {
                this.change_authorization(true); this.visible_login_form(false);
        } else {
            this.change_authorization(false);
            this.visible_login_form(true);
        }
    }

    login(login, password)
    {
        let ObjectRequest = {
            authorization: {
                login: login,
                password: password
            }
        };
        this.__post_http_request(
            ObjectRequest,
            "/login",
            (response) => {
                this.successful_login(response)
            }
        );
    }

    logout()
    {
        this.__post_http_request(
            null,
            "/logout",
            null
        );
        localStorage.removeItem("auth_token");
        localStorage.removeItem("auth_id");
        this.change_authorization(false);
        this.visible_login_form(true);
    }

    registration(in_name, in_age, in_password, in_phone)
    {
        let ObjectRequest = {
            name: in_name,
            age: in_age,
            password: in_password,
            phone: in_phone,
            registration: true
        };
        this.__post_http_request(
            ObjectRequest,
            "/registration",
            (response) => {
                this.successful_login(response)
            }
        );
    }

    load_save(CALLBACK)
    {
        this.__post_http_request(
            null,
            "/get_save",
            CALLBACK
        );
    }

    get_assortment_of_shops(CALLBACK)
    {
        this.__post_http_request(
            null,
            "/get_assortment_of_shops",
            CALLBACK
        );
    }

    buy_item(in_item_id, in_item_type, CALLBACK)
    {
        let ObjectRequest = {
            item_id: parseInt(in_item_id),
            item_type: in_item_type,
        };
        this.__post_http_request(
            ObjectRequest,
            "/buy_item",
            CALLBACK
        );
    }

    get_assortment_antagonists(CALLBACK)
    {
        this.__post_http_request(
            null,
            "/get_assortment_antagonists",
            CALLBACK
        );
    }

    select_antagonist(in_antagonist_id, CALLBACK)
    {
        let ObjectRequest = {
            antagonist_id: in_antagonist_id,
        };
        this.__post_http_request(
            ObjectRequest,
            "/select_antagonist",
            CALLBACK
        );
    }

    handle_send_step(in_attack, in_block, CALLBACK)
    {
        let ObjectRequest = {
            attack_area: in_attack,
            block_area: in_block
        };
        this.__post_http_request(
            ObjectRequest,
            "/get_rate",
            CALLBACK
        );
    }

    successful_login(response)
    {
        localStorage["auth_id"] = response.id;
        localStorage["auth_token"] = response.token;
        this.change_authorization(true);
        this.visible_login_form(false);
    }

    __post_http_request(ObjectPackage, API, CALLBACK)
    {
        let PackagePOST = {};
        let AuthorizationIsSet = false;

        if (ObjectPackage !== null) {
            if (ObjectPackage.authorization !== undefined && ObjectPackage.authorization !== null) {
                PackagePOST.login = ObjectPackage.authorization.login;
                PackagePOST.password = ObjectPackage.authorization.password;
                AuthorizationIsSet = true;
            } else if (ObjectPackage.registration !== undefined && ObjectPackage.registration !== null){
                AuthorizationIsSet = true;
            }
        }
        if (!AuthorizationIsSet) {
            if (localStorage["auth_token"] !== undefined && localStorage["auth_id"] !== undefined) {
                PackagePOST.id = localStorage["auth_id"];
                PackagePOST.token = localStorage["auth_token"];
                AuthorizationIsSet = true;
            }
        }
        if (AuthorizationIsSet) {
            if (ObjectPackage !== null) {
                for (let key in ObjectPackage) {
                    if (key !== "authorization" && key !== "registration") {
                        PackagePOST[key] = ObjectPackage[key];
                    }
                }
            }
            fetch(this.BackEndDomain + API, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'POST',
                body: JSON.stringify(PackagePOST)
            })
                .then(response => {
                    if (response.status >= 200 && response.status <= 299) {
                        return response.json();
                    } else if (response.status === 401){
                        this.visible_login_form(true);
                        this.change_authorization(false);
                        return null;
                    }
                }).then(body => {
                if (body !== null) {
                    if (CALLBACK !== null) {
                        CALLBACK(body);
                    }
                }
            });
        } else {
            this.change_authorization(false);
            this.visible_login_form(true);
        }
    }
}
